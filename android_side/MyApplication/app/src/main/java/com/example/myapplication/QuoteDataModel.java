package com.example.myapplication;

public class QuoteDataModel {
    public static final int TITLE_QUOTE = 0;
    public static final int TOTAL_QUOTE = 1;
    public static final int VALUE_QUOTE = 2;


    private String title_nombre, title_quantite, title_prixUnitaire;
    private String nombre, quantite, prixUnitaire;
    public String total;
    public int type;



    public QuoteDataModel(int type,String total, String nombre, String quantite, String prixUnitaire, String title_nombre, String title_quantite, String title_prixUnitaire) {

        this.title_nombre=title_nombre;
        this.title_quantite= title_quantite;
        this.title_prixUnitaire= title_prixUnitaire;
        this.type=type;
        this.nombre= nombre;
        this.prixUnitaire= prixUnitaire;
        this.quantite= quantite;
        this.type=type;
        this.total= total;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getQuantité() {
        return quantite;
    }

    public void setQuantité(String quantité) {
        this.quantite = quantité;
    }

    public String getPrixUnitaire() {
        return prixUnitaire;
    }

    public void setPrixUnitaire(String prixUnitaire) {
        this.prixUnitaire = prixUnitaire;
    }
}
