package com.example.myapplication;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;


public class fragment_arti_list extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private RecyclerView recyclerView;
    private ArtiList_CustomAdapter artiList_customAdapter;
    private List<UserDataModel> userdatamodel = new ArrayList<>();



    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;


    public fragment_arti_list() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        UserDataModel userDataModel= new UserDataModel("lionel","plombier","Bamenda");
        userdatamodel.add(userDataModel);

        userDataModel= new UserDataModel("lionel","plombier","Bamenda");
        userdatamodel.add(userDataModel);

        userDataModel= new UserDataModel("lionel","plombier","Bamenda");
        userdatamodel.add(userDataModel);

        userDataModel= new UserDataModel("lionel","plombier","Bamenda");
        userdatamodel.add(userDataModel);

        userDataModel= new UserDataModel("lionel","plombier","Bamenda");
        userdatamodel.add(userDataModel);

        userDataModel= new UserDataModel("lionel","plombier","Bamenda");
        userdatamodel.add(userDataModel);

        userDataModel= new UserDataModel("lionel","plombier","Bamenda");
        userdatamodel.add(userDataModel);

        userDataModel= new UserDataModel("lionel","plombier","Bamenda");
        userdatamodel.add(userDataModel);


        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_fragment_arti_list, container, false);
        recyclerView = view.findViewById(R.id.my_recycler_view);
        artiList_customAdapter= new ArtiList_CustomAdapter(userdatamodel);
        RecyclerView.LayoutManager layoutManager= new GridLayoutManager(getActivity(), 2);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(artiList_customAdapter);


        return  view;
    }

}
