package com.example.myapplication;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class QuoteAdapter extends RecyclerView.Adapter {
    private List<QuoteDataModel> quoteDataModels;

    public EditText nombre1, prix_unitaire1, quantité1;


    public QuoteAdapter(List<QuoteDataModel> quoteDataModels) {
        this.quoteDataModels= quoteDataModels;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view;
        switch (viewType) {
            case QuoteDataModel.TITLE_QUOTE:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.titlequotes_cardview, parent, false);
                return new TitleQuoteViewHolder(view);

            case QuoteDataModel.VALUE_QUOTE:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.quotes_cardview, parent, false);
                return new QuoteViewHolder(view);

            case QuoteDataModel.TOTAL_QUOTE:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.totalquotes_cardview, parent, false);
                return new TotalQuoteViewHolder(view);
        }
        return null;

    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int listPosition) {
        QuoteDataModel quoteDataModel= quoteDataModels.get(listPosition);
        if (quoteDataModel!= null){

            switch (quoteDataModel.type) {
                case QuoteDataModel.TITLE_QUOTE:
                    /*((TitleQuoteViewHolder) holder).nombre.setText("quoteDataModel.getNombre()");
                    ((TitleQuoteViewHolder) holder).quantité.setText("quoteDataModel.getQuantité()");
                    ((TitleQuoteViewHolder) holder).prix_unitaire.setText("quoteDataModel.getPrixUnitaire()");

                     */

                    ((TitleQuoteViewHolder) holder).nombre.setText("Nombre");
                    ((TitleQuoteViewHolder) holder).quantité.setText("Quantité");
                    ((TitleQuoteViewHolder) holder).prix_unitaire.setText("Prix");

                    break;

                case QuoteDataModel.VALUE_QUOTE:

                    /*((QuoteViewHolder) holder).nombre1.setHint(quoteDataModel.getNombre());
                    ((QuoteViewHolder) holder).quantité1.setHint(quoteDataModel.getQuantité());
                    ((QuoteViewHolder) holder).prix_unitaire1.setHint(quoteDataModel.getPrixUnitaire());

                     */

                    ((QuoteViewHolder) holder).nombre1.setText("25");
                    ((QuoteViewHolder) holder).quantité1.setText("25");
                    ((QuoteViewHolder) holder).prix_unitaire1.setText("25");




                    break ;
                case QuoteDataModel.TOTAL_QUOTE:
                   //((TotalQuoteViewHolder) holder).total_num.setText(quoteDataModel.getPrixUnitaire());
                    ((TotalQuoteViewHolder) holder).total_num.setText("2355");

                    break ;

            }
        }
        /*holder.nombre.setText(quoteDataModel.getNombre());
        holder.quantité.setText(quoteDataModel.getQuantité());
        holder.prix_unitaire.setText(quoteDataModel.getPrixUnitaire());

         */

    }


    @Override
    public int getItemCount() {
        return quoteDataModels.size();
    }


    @Override
    public int getItemViewType(int position) {

        switch (quoteDataModels.get(position).type) {
            case 0:
                return QuoteDataModel.TITLE_QUOTE;
            case 1:
                return QuoteDataModel.TOTAL_QUOTE;
            case 2:
                return QuoteDataModel.VALUE_QUOTE;
            default:
                return -1;
        }
    }

    class QuoteViewHolder extends RecyclerView.ViewHolder{

        public EditText nombre1, prix_unitaire1, quantité1;

        public QuoteViewHolder(@NonNull View itemView) {
            super(itemView);

            nombre1= itemView.findViewById(R.id.unit_quote);
            prix_unitaire1= itemView.findViewById(R.id.price_quote);
            quantité1= itemView.findViewById(R.id.quantity_quote);
        }
    }


    class TitleQuoteViewHolder extends RecyclerView.ViewHolder{

        TextView nombre, prix_unitaire, quantité;

        public TitleQuoteViewHolder(@NonNull View itemView) {
            super(itemView);

            nombre= itemView.findViewById(R.id.quote_num);
            prix_unitaire= itemView.findViewById(R.id.quote_price);
            quantité= itemView.findViewById(R.id.quote_quant);
        }
    }

    class TotalQuoteViewHolder extends RecyclerView.ViewHolder{

        EditText total_num;
        TextView total;

        public TotalQuoteViewHolder(@NonNull View itemView) {
            super(itemView);

            total_num= itemView.findViewById(R.id.quote_total_num);
            total= itemView.findViewById(R.id.quote_total);
        }
    }



}

