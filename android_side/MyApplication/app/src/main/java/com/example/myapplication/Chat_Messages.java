package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;

import org.jivesoftware.smack.AbstractXMPPConnection;
import org.jivesoftware.smack.ConnectionConfiguration;
import org.jivesoftware.smack.SASLAuthentication;
import org.jivesoftware.smack.SmackConfiguration;
import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.chat2.Chat;
import org.jivesoftware.smack.chat2.ChatManager;
import org.jivesoftware.smack.chat2.IncomingChatMessageListener;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.tcp.XMPPTCPConnection;
import org.jivesoftware.smack.tcp.XMPPTCPConnectionConfiguration;
import org.jxmpp.jid.DomainBareJid;
import org.jxmpp.jid.EntityBareJid;
import org.jxmpp.jid.impl.JidCreate;
import org.jxmpp.stringprep.XmppStringprepException;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLSession;

public class Chat_Messages extends AppCompatActivity implements View.OnClickListener {


    private ImageView send;
    private EditText editText;
    private AbstractXMPPConnection mConnection;
    public RecyclerView mRecyclerView;
    public ChatAdapter mAdapter;
    public List<ChatDataModel> chatAdapters= new ArrayList<>();
    public static final String TAG = MainActivity.class.getSimpleName();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat__messages);

        send= findViewById(R.id.chat_Button);
        editText= findViewById(R.id.chat_editText);
        mRecyclerView= findViewById(R.id.chat_recyclerview);

        LinearLayoutManager manager = new LinearLayoutManager(this);
        DividerItemDecoration decoration = new DividerItemDecoration(this,manager.getOrientation());

        mRecyclerView.addItemDecoration(decoration);
        mRecyclerView.setLayoutManager(manager);
        mRecyclerView.setAdapter(mAdapter);

        setConnection();

        send.setOnClickListener(this);


    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){

            case R.id.chat_Button:
                //get de text message
                String msg_body= editText.getText().toString();

                if(msg_body.length() > 0 ){

                    //Entitiy bare jid consits of username to whom you want to send message and domain of server
                    //to check domain of server follow me

                    sendMessage(msg_body,"lionelruto@localhost");
                }

        }




    }




    private void sendMessage(String messageSend, String entityBareId) {

        EntityBareJid jid = null;
        try {
            jid = JidCreate.entityBareFrom(entityBareId);
        } catch (XmppStringprepException e) {
            e.printStackTrace();
        }

        if(mConnection != null) {

            ChatManager chatManager = ChatManager.getInstanceFor(mConnection);
            Chat chat = chatManager.chatWith(jid);
            Message newMessage = new Message();
            newMessage.setBody(messageSend);

            try {
                chat.send(newMessage);

                ChatDataModel data = new ChatDataModel("send",messageSend);
                chatAdapters.add(data);
                mAdapter = new ChatAdapter(chatAdapters);
                mRecyclerView.setAdapter(mAdapter);




            } catch (SmackException.NotConnectedException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }










    private void setConnection(){
        // Create the configuration for this new connection

        //this function or code given in official documention give an error in openfire run locally to solve this error
        //first off firewall
        //then follow my steps

        new Thread(){
            @Override
            public void run() {



                InetAddress addr = null;
                try {

                    // inter your ip4address now checking it
                    addr = InetAddress.getByName("localhost");
                } catch (UnknownHostException e) {
                    e.printStackTrace();
                }
                HostnameVerifier verifier = new HostnameVerifier() {
                    @Override
                    public boolean verify(String hostname, SSLSession session) {
                        return false;
                    }
                };
                DomainBareJid serviceName = null;
                try {
                    serviceName = JidCreate.domainBareFrom("localhost");
                } catch (XmppStringprepException e) {
                    e.printStackTrace();
                }


                XMPPTCPConnectionConfiguration config = XMPPTCPConnectionConfiguration.builder()

                        .setUsernameAndPassword("admin","Lionelruto20")
                        .setPort(5222)
                        .setSecurityMode(ConnectionConfiguration.SecurityMode.disabled)
                        .setXmppDomain(serviceName)
                        .setHostnameVerifier(verifier)
                        .setHostAddress(addr)
                        .setDebuggerEnabled(true)
                        .build();
                mConnection = new XMPPTCPConnection(config);


                try {
                    mConnection.connect();
                    // all these proceedure also thrown error if you does not seperate this thread now we seperate thread create
                    mConnection.login();

                    if(mConnection.isAuthenticated() && mConnection.isConnected()){
                        //now send message and receive message code here

                        Log.e(TAG, "run: auth done and connected successfully" );
                        ChatManager chatManager = ChatManager.getInstanceFor(mConnection);
                        chatManager.addIncomingListener(new IncomingChatMessageListener() {
                            @Override
                            public void newIncomingMessage(EntityBareJid from, Message message, Chat chat) {
                                Log.e(TAG,"New message from " + from + ": " + message.getBody());

                                ChatDataModel data = new ChatDataModel("received",message.getBody().toString());
                                chatAdapters.add(data);

                                //now update recyler view
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        //this ui thread important otherwise error occur

                                        mAdapter = new ChatAdapter(chatAdapters);
                                        mRecyclerView.setAdapter(mAdapter);
                                    }
                                });
                            }
                        });

                    }




                } catch (SmackException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (XMPPException e) {
                    e.printStackTrace();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        } .start();

    }


}
