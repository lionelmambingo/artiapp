package com.example.myapplication;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.ViewPager;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;

import com.google.android.material.bottomnavigation.BottomNavigationView;

public class ArtiList extends AppCompatActivity {

    private int NUMBER_PAGES= 4;
    MenuItem prevMenuItem;
    private BottomNavigationView bottomNavigationView;
    private ViewPager viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_arti_list);

        bottomNavigationView = (BottomNavigationView) findViewById(R.id.navigation);
        viewPager = (ViewPager) findViewById(R.id.viewpager_list);
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {

                switch (item.getItemId()) {
                    case R.id.item_home:
                        viewPager.setCurrentItem(0);
                        break;
                    case R.id.item_chat:
                        viewPager.setCurrentItem(1);
                        break;
                    case R.id.item_search:
                        viewPager.setCurrentItem(2);
                        break;

                    case R.id.item_person:
                        viewPager.setCurrentItem(3);
                        break;

                }

                return false;
            }
        });



        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if (prevMenuItem != null) {
                    prevMenuItem.setChecked(false);
                } else {
                    bottomNavigationView.getMenu().getItem(0).setChecked(false);
                }
                Log.d("page", "onPageSelected: " + position);
                bottomNavigationView.getMenu().getItem(position).setChecked(true);
                prevMenuItem = bottomNavigationView.getMenu().getItem(position);

            }


            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });



        setupViewPager(viewPager);

    }




    private class FragmentstateAdapter extends FragmentStatePagerAdapter {

        private FragmentstateAdapter(FragmentManager fa){
            super(fa);

        }

        @Override
        public Fragment getItem(int position) {
            switch (position){

                case 0:
                    return new fragment_arti_list();
                case 1:
                    Intent intent = new Intent(getApplication(), User_Page.class);
                    startActivity(intent);
                case 2 :
                    return new Intro_Slider3();
                case 3 :
                    return new Intro_Slider3();



            }
            return null;
        }

        @Override
        public int getCount() {
            return NUMBER_PAGES;
        }
    }



    private void setupViewPager(ViewPager mviewPager) {
        FragmentstateAdapter viewPagerAdapter = new FragmentstateAdapter(getSupportFragmentManager());
        mviewPager.setAdapter(viewPagerAdapter);
        mviewPager.setSaveEnabled (false);

    }

}
