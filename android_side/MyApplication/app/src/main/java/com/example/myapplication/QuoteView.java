package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.pdf.PdfDocument;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.util.LruCache;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;


import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

public class QuoteView extends AppCompatActivity {

    RecyclerView mRecyclerView;
    private QuoteAdapter quoteAdapter;
    int duration = Toast.LENGTH_SHORT;

    private List<QuoteDataModel> quotedatamodel= new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quote_view);

        Toolbar myToolbar = findViewById(R.id.toolbar3);
        setSupportActionBar(myToolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        mRecyclerView= findViewById(R.id.quoteRecycler);

        QuoteDataModel quoteDataModel= new QuoteDataModel(0,"0", "0", "0","0","nombre","quantitte","prix");
        quotedatamodel.add(quoteDataModel);

        QuoteDataModel quoteDataModel2= new QuoteDataModel(2,"0", "0", "0","0","","","");
        quotedatamodel.add(quoteDataModel2);

        QuoteDataModel quoteDataModel1= new QuoteDataModel(1,"2330", "0", "0","0","","","");
        quotedatamodel.add(quoteDataModel1);


        FloatingActionButton fab = findViewById(R.id.quotes_floatbtn);

        // Inflate the layout for this fragment
        quoteAdapter= new QuoteAdapter(quotedatamodel);
        RecyclerView.LayoutManager layoutManager= new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(layoutManager);
        mRecyclerView.setAdapter(quoteAdapter);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int item_number= quotedatamodel.size();
                Toast toast = Toast.makeText(getApplicationContext(), "item_number", duration);
                toast.show();
                int positionIndex= item_number-1;
                QuoteDataModel quoteDataModel1= new QuoteDataModel(2,"2330", "0", "0","0","","","");
                quotedatamodel.add(positionIndex, quoteDataModel1);
                quoteAdapter.notifyItemInserted(positionIndex);

            }
        });



    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.quote_items, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.item_send:
                /* DO EDIT */
                Context context = getApplicationContext();
                CharSequence text = "Hello toast!";
                Toast toast = Toast.makeText(context, text, duration);
                toast.show();
                tela();

            case R.id.fav_btns:
                openGeneratedPDF();

        }
        return super.onOptionsItemSelected(item);
    }





    public void tela (){ // create a new document

        Bitmap recycler_view_bm =     getScreenshotFromRecyclerView(mRecyclerView);
        // saving pdf document to sdcard
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy - HH-mm-ss",Locale.getDefault());
        String pdfName = "Devis"
                + sdf.format(Calendar.getInstance().getTime()) + ".pdf";

        // all created files will be saved at path /sdcard/PDFDemo_AndroidSRC/
        File downloadFolder = getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS);
        File pdfFile = new File(downloadFolder.getPath(), pdfName);


        try {


            pdfFile.createNewFile();
            FileOutputStream fOut = new FileOutputStream(pdfFile);
            PdfDocument document = new PdfDocument();
            PdfDocument.PageInfo pageInfo = new
                    PdfDocument.PageInfo.Builder(recycler_view_bm.getWidth(), recycler_view_bm.getHeight(), 1).create();
            PdfDocument.Page page = document.startPage(pageInfo);
            recycler_view_bm.prepareToDraw();
            Canvas c;
            c = page.getCanvas();
            c.drawBitmap(recycler_view_bm,0,0,null);
            document.finishPage(page);
            try {
                document.writeTo(fOut);
            } catch (IOException ex) {
                ex.printStackTrace();
            }
            document.close();
        /*Snackbar snackbar = Snackbar
                .make(equipmentsRecordActivityLayout, "PDF generated successfully.", Snackbar.LENGTH_LONG)
                .setAction("Open", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        openPDFRecord(pdfFile);
                    }
                });

        snackbar.show();*/

        }catch (IOException e) {
            e.printStackTrace();
        }
       /* if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT)

        {
            PdfDocument document = new PdfDocument();

            getScreenshotFromRecyclerView(mRecyclerView);
            content = mRecyclerView;
            content.setBackgroundColor(Color.parseColor("#303030"));


            PdfDocument.PageInfo pageInfo = new PdfDocument.PageInfo.Builder(wil,
                    height, 1).create();

            // create a new page from the PageInfo
            PdfDocument.Page page = document.startPage(pageInfo);

            // repaint the user's text into the page
            content.draw(page.getCanvas());

            // do final processing of the page
            document.finishPage(page);

            // saving pdf document to sdcard
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy - HH-mm-ss",Locale.getDefault());
            String pdfName = "Revisões_"
                    + sdf.format(Calendar.getInstance().getTime()) + ".pdf";

            // all created files will be saved at path /sdcard/PDFDemo_AndroidSRC/
            File outputFile = new File(Environment.getExternalStorageDirectory().getPath(), pdfName);

            try {
                outputFile.createNewFile();
                OutputStream out = new FileOutputStream(outputFile);
                document.writeTo(out);
                document.close();
                out.close();
                Toast.makeText(this,"PDF gerado com sucesso",Toast.LENGTH_SHORT).show();
                Log.i("Gerou", "pdf");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        */
    }





    public Bitmap getScreenshotFromRecyclerView(RecyclerView view) {
        RecyclerView.Adapter adapter = view.getAdapter();
        Bitmap bigBitmap = null;
        if (adapter != null) {
            int size = adapter.getItemCount();
            int height = 0;
            Paint paint = new Paint();
            int iHeight = 0;
            final int maxMemory = (int) (Runtime.getRuntime().maxMemory() / 1024);

            // Use 1/8th of the available memory for this memory cache.
            final int cacheSize = maxMemory / 8;
            LruCache<String, Bitmap> bitmaCache = new LruCache<>(cacheSize);
            for (int i = 0; i < size; i++) {
                RecyclerView.ViewHolder holder = adapter.createViewHolder(view, adapter.getItemViewType(i));
                adapter.onBindViewHolder(holder, i);
                holder.itemView.measure(View.MeasureSpec.makeMeasureSpec(view.getWidth(), View.MeasureSpec.EXACTLY),
                        View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED));
                holder.itemView.layout(0, 0, holder.itemView.getMeasuredWidth(), holder.itemView.getMeasuredHeight());
                holder.itemView.setDrawingCacheEnabled(true);
                holder.itemView.buildDrawingCache();
                Bitmap drawingCache = holder.itemView.getDrawingCache();
                if (drawingCache != null) {

                    bitmaCache.put(String.valueOf(i), drawingCache);
                }

                height += holder.itemView.getMeasuredHeight();
            }

            bigBitmap = Bitmap.createBitmap(view.getMeasuredWidth(), height, Bitmap.Config.ARGB_8888);
            Canvas bigCanvas = new Canvas(bigBitmap);
            bigCanvas.drawColor(Color.WHITE);

            for (int i = 0; i < size; i++) {
                Bitmap bitmap = bitmaCache.get(String.valueOf(i));
                bigCanvas.drawBitmap(bitmap, 0f, iHeight, paint);
                iHeight += bitmap.getHeight();
                bitmap.recycle();
            }

        }
        return bigBitmap;
    }

    private void openGeneratedPDF(){
        File downloadFolder = getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS);
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy - HH-mm-ss",Locale.getDefault());

        String pdfName = "Devis"
                + sdf.format(Calendar.getInstance().getTime()) + ".pdf";

        File file = new File( downloadFolder,pdfName);
        if (file.exists())
        {
            Intent intent=new Intent(Intent.ACTION_VIEW);
            Uri uri = Uri.fromFile(file);
            intent.setDataAndType(uri, "application/pdf");
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

            try
            {
                startActivity(intent);
            }
            catch(ActivityNotFoundException e)
            {
                Toast.makeText(this, "No Application available to view pdf", Toast.LENGTH_LONG).show();
            }
        }
    }
}



