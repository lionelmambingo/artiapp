package com.example.myapplication;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class Favorite_list_Adapter extends RecyclerView.Adapter<Favorite_list_Adapter.FavoriteList_ViewHolder> {

    private List<FavoriteDataModel> favoriteDataModels;


    public Favorite_list_Adapter(List<FavoriteDataModel> favoriteDataModels) {
        this.favoriteDataModels= favoriteDataModels;
    }

    @NonNull
    @Override
    public FavoriteList_ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View itemview= LayoutInflater.from(parent.getContext())
                .inflate(R.layout.favorite_arti_cardview, parent, false);
        return new FavoriteList_ViewHolder(itemview);
    }

    @Override
    public void onBindViewHolder(@NonNull FavoriteList_ViewHolder holder, int position) {

        FavoriteDataModel fav_model= favoriteDataModels.get(position);
        holder.fav_name.setText(fav_model.getFav_name());

    }

    @Override
    public int getItemCount() {
        return favoriteDataModels.size();
    }


    public class FavoriteList_ViewHolder extends RecyclerView.ViewHolder{

        private TextView fav_name;

        public FavoriteList_ViewHolder(@NonNull View itemView) {
            super(itemView);

            fav_name= itemView.findViewById(R.id.fav_name);
        }
    }
}
