package com.example.myapplication;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public class FavoriteList_Fragment extends Fragment {

    private RecyclerView recyclerView;
    private Favorite_list_Adapter favorite_list_adapter;
    private List<FavoriteDataModel> user_favoriteDataModels = new ArrayList<>();


    public FavoriteList_Fragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                   Bundle savedInstanceState){

        FavoriteDataModel favoriteDataModel= new FavoriteDataModel("lionel");
        user_favoriteDataModels.add(favoriteDataModel);

        FavoriteDataModel favoriteDataModel2= new FavoriteDataModel("lionel");
        user_favoriteDataModels.add(favoriteDataModel2);

        FavoriteDataModel favoriteDataModel22= new FavoriteDataModel("lionel");
        user_favoriteDataModels.add(favoriteDataModel22);



        View v = inflater.inflate(R.layout.fragmentfavoritel_list, container, false);
        recyclerView = v.findViewById(R.id.fav_recyclerview);
        favorite_list_adapter= new Favorite_list_Adapter(user_favoriteDataModels);
        RecyclerView.LayoutManager layoutManager= new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(favorite_list_adapter);
        return v;

    }
}
