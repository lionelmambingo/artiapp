package com.example.myapplication;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;
import java.util.zip.Inflater;

public class Disc_List_Fragment extends Fragment {
    private RecyclerView recyclerView;
    private Disc_custom_Adapter disc_custom_adapter;
    private List<List_DiscDataModel> userlistDataModel= new ArrayList<>() ;


    public Disc_List_Fragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                Bundle savedInstanceState){


        List_DiscDataModel list_discDataModel= new List_DiscDataModel("lionel", "gommeur");
        userlistDataModel.add(list_discDataModel);

        List_DiscDataModel list_discDataModel1= new List_DiscDataModel("lionel", "gommeur");
        userlistDataModel.add(list_discDataModel1);

        List_DiscDataModel list_discDataModel2= new List_DiscDataModel("lionel", "gommeur");
        userlistDataModel.add(list_discDataModel2);

        List_DiscDataModel list_discDataModel3= new List_DiscDataModel("lionel", "gommeur");
        userlistDataModel.add(list_discDataModel3);

        List_DiscDataModel list_discDataModel4= new List_DiscDataModel("lionel", "gommeur");
        userlistDataModel.add(list_discDataModel4);

        List_DiscDataModel list_discDataModel5= new List_DiscDataModel("lionel", "gommeur");
        userlistDataModel.add(list_discDataModel5);

        List_DiscDataModel list_discDataModel6= new List_DiscDataModel("lionel", "gommeur");
        userlistDataModel.add(list_discDataModel6);

        List_DiscDataModel list_discDataModel7= new List_DiscDataModel("lionel", "gommeur");
        userlistDataModel.add(list_discDataModel7);
        List_DiscDataModel list_discDataModel8= new List_DiscDataModel("lionel", "gommeur");
        userlistDataModel.add(list_discDataModel8);

        List_DiscDataModel list_discDataModel9= new List_DiscDataModel("lionel", "gommeur");
        userlistDataModel.add(list_discDataModel9);

        List_DiscDataModel list_discDataModel10= new List_DiscDataModel("lionel", "gommeur");
        userlistDataModel.add(list_discDataModel10);
        List_DiscDataModel list_discDataModel11= new List_DiscDataModel("lionel", "gommeur");
        userlistDataModel.add(list_discDataModel11);

        List_DiscDataModel list_discDataModel12= new List_DiscDataModel("lionel", "gommeur");
        userlistDataModel.add(list_discDataModel12);

        List_DiscDataModel list_discDataModel13= new List_DiscDataModel("lionel", "gommeur");
        userlistDataModel.add(list_discDataModel13);



        View v = inflater.inflate(R.layout.fragment_fragment_arti_list, container, false);
        recyclerView = v.findViewById(R.id.my_recycler_view);
        disc_custom_adapter= new Disc_custom_Adapter(userlistDataModel);
        RecyclerView.LayoutManager layoutManager= new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(disc_custom_adapter);
        return v;

    }
}
