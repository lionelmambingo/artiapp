package com.example.myapplication;

import android.widget.TextView;

public class List_DiscDataModel {

    private String list_name, list_last_disc;

    public List_DiscDataModel(String list_last_disc, String list_name) {

        this.list_last_disc= list_last_disc;
        this.list_name= list_name;


    }

    public void setList_name(String list_name) {
        this.list_name = list_name;
    }

    public void setList_last_disc(String list_last_disc) {
        this.list_last_disc = list_last_disc;
    }

    public String getList_name() {
        return list_name;
    }

    public String getList_last_disc() {
        return list_last_disc;
    }
}

