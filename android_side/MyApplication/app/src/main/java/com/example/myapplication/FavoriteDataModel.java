package com.example.myapplication;

public class FavoriteDataModel {

    private String fav_name;

    public FavoriteDataModel(String fav_name) {
        this.fav_name= fav_name;
    }

    public void setFav_name(String fav_name) {
        this.fav_name = fav_name;
    }

    public String getFav_name() {
        return fav_name;
    }
}
