package com.example.myapplication;

public class ChatDataModel {

    private String message,Jid;

    public String getMessage() {
        return message;
    }

    public String getJid() {
        return Jid;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setJid(String jid) {
        Jid = jid;
    }

    public ChatDataModel(String message, String Jid) {
        this.message= message;
        this.Jid= Jid;

    }
}
