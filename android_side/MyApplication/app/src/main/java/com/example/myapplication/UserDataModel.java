package com.example.myapplication;

import android.graphics.Bitmap;

public class UserDataModel {

    private String nom, profession, city;
    //private Bitmap image;

    public String getNom() {
        return nom;
    }

    public String getCity() {
        return city;
    }

    public String getProfession() {
        return profession;
    }

    /*public Bitmap getImage() {
        return null;
    }

     */


    public void setCity(String city) {
        this.city = city;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    /*public void setImage(Bitmap image) {
        this.image = image;
    }

     */

    public void setProfession(String profession) {
        this.profession= profession;

    }


    public UserDataModel(String nom, String profession, String city/*, Bitmap image*/){

        this.nom= nom;
        this.profession= profession;
        this.city= city;
        //this.image= image;

    }
}
