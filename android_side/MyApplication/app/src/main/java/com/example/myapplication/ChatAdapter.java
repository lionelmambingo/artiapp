package com.example.myapplication;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class ChatAdapter extends RecyclerView.Adapter<ChatAdapter.ChatViewHolder> {

    private List<ChatDataModel> chatDataModels;

    public ChatAdapter(List<ChatDataModel> chatDataModels) {

        this.chatDataModels= chatDataModels;

    }

    @NonNull
    @Override
    public ChatViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView= LayoutInflater.from(parent.getContext())
                .inflate(R.layout.message_receive_cardview, parent, false);
        return new ChatAdapter.ChatViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ChatViewHolder holder, int position) {
        ChatDataModel chatDataModel= chatDataModels.get(position);
        holder.message.setText(chatDataModel.getMessage());

    }

    @Override
    public int getItemCount() {
        return chatDataModels.size();
    }


    public class ChatViewHolder extends RecyclerView.ViewHolder{
        public EditText message;

        public ChatViewHolder(@NonNull View itemView) {
            super(itemView);

            message= itemView.findViewById(R.id.chat_editText);
        }
    }
}
