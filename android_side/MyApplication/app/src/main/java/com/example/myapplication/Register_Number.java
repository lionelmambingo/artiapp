package com.example.myapplication;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class Register_Number extends AppCompatActivity implements View.OnClickListener {

    private EditText number1; String value1; private Button btn_next;
    private EditText number2; String value2;
    private EditText number3; String value3;
    private EditText number4; String value4;
    private EditText number5; String value5;
    private EditText number6; String value6;
    private EditText number7; String value7;
    private EditText number8; String value8;
    private EditText number9; String value9;

    public ArrayList <String> numList;
    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register__number);
        numList = new ArrayList();
        btn_next= findViewById(R.id.btn_next);

        number1= findViewById(R.id.first_number);
        number2= findViewById(R.id.second_number);
        number3= findViewById(R.id.third_number);
        number4= findViewById(R.id.fourth_number);
        number5= findViewById(R.id.fifth_number);
        number6= findViewById(R.id.sixth_number);
        number7= findViewById(R.id.seventh_number);
        number8= findViewById(R.id.eight_number);
        number9= findViewById(R.id.ninth_number);

        /**
         *dans cette partie, pour chaque EditText cliqué, on le rend focusable avec requestFocus()
         */

        number1.setOnClickListener(this);
        number2.setOnClickListener(this);
        number3.setOnClickListener(this);
        number4.setOnClickListener(this);
        number5.setOnClickListener(this);
        number6.setOnClickListener(this);
        number7.setOnClickListener(this);
        number8.setOnClickListener(this);
        number9.setOnClickListener(this);

        number1.requestFocus();


        /**
        *dans cette partie, on fait un addTextChangeListener sur chaque EdiText pour qu'apres avoir saisi la valeur
         * on va sur le edittext suivant. en attendant un code mieux fourni on fait d'abord avec
         */

        number1.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String val= number1.getText().toString();
                if (val.trim().equals("")){
                    number1.requestFocus();

                }else {
                    number2.requestFocus();
                    numList.add(val);

                }


            }
        });


        number2.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String val= number2.getText().toString();
                if (val.trim().equals("")){
                    number2.requestFocus();

                }else {
                    number3.requestFocus();
                    numList.add(val);

                }


            }
        });


        number3.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String val= number3.getText().toString();

                if (val.trim().equals("")){
                    number3.requestFocus();

                }else {
                    number4.requestFocus();
                    numList.add(val);

                }


            }
        });


        number4.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String val= number4.getText().toString();

                if (val.trim().equals("")){
                    number4.requestFocus();

                }else {
                    number5.requestFocus();
                    numList.add(val);

                }


            }
        });


        number5.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String val= number5.getText().toString();
                if (val.trim().equals("")){
                    number5.requestFocus();

                }else {
                    number6.requestFocus();
                    numList.add(val);

                }


            }
        });

        number6.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String val= number6.getText().toString();
                if (val.trim().equals("")){
                    number6.requestFocus();

                }else {
                    number7.requestFocus();
                    numList.add(val);

                }


            }
        });


        number7.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String val= number7.getText().toString();
                if (val.trim().equals("")){
                    number7.requestFocus();

                }else {
                    number8.requestFocus();
                    numList.add(val);

                }


            }
        });


        number8.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String val= number8.getText().toString();
                if (val.trim().equals("")){
                    number8.requestFocus();

                }else {
                    number9.requestFocus();
                    numList.add(val);

                }


            }
        });


        number9.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String val= number9.getText().toString();
                if (val.trim().equals("")){
                    number9.requestFocus();

                }else {
                    number9.requestFocus();
                    numList.add(val);

                }


            }
        });

        btn_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (numList.size()<8){
                    Toast.makeText(getApplicationContext(), "veuillez saisir à nouveau votre numéro",Toast.LENGTH_LONG).show();
                }else{
                    Intent intent= new Intent(getApplicationContext(), Receive_MessageCodeRegristration.class);
                    intent.putStringArrayListExtra("number", numList);
                    startActivity(intent);
                }
            }
        });





    }




    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.first_number:
                number1.requestFocus();
            case R.id.second_number:
                number2.requestFocus();
            case R.id.third_number:
                number3.requestFocus();
            case R.id.fourth_number:
                number4.requestFocus();
            case R.id.fifth_number:
                number5.requestFocus();
            case R.id.sixth_number:
                number6.requestFocus();
            case R.id.seventh_number:
                number7.requestFocus();
            case R.id.eight_number:
                number8.requestFocus();
            case R.id.ninth_number:
                number9.requestFocus();

        }

    }

}
