package com.example.myapplication;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;
import java.util.zip.Inflater;

public class Disc_custom_Adapter extends RecyclerView.Adapter<Disc_custom_Adapter.Disc_ViewHolder> {

    private List<List_DiscDataModel> disc_list;

    public Disc_custom_Adapter(List<List_DiscDataModel> disc_list){
        this.disc_list= disc_list;

    }
    @NonNull
    @Override
    public Disc_ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View itemView= LayoutInflater.from(parent.getContext())
                .inflate(R.layout.disc_list_cardview, parent, false);
        return new Disc_ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull Disc_ViewHolder holder, int position) {
        List_DiscDataModel list_discDataModel= disc_list.get(position);
        holder.nom.setText(list_discDataModel.getList_name());
        holder.last_dis.setText(list_discDataModel.getList_last_disc());


    }

    @Override
    public int getItemCount() {
        return disc_list.size();
    }


    public class Disc_ViewHolder extends RecyclerView.ViewHolder{

        public TextView nom, last_dis;

        public Disc_ViewHolder(@NonNull View itemView) {
            super(itemView);
            nom =itemView.findViewById(R.id.disc_name);
            last_dis= itemView.findViewById(R.id.disc_last);
        }
    }
}
