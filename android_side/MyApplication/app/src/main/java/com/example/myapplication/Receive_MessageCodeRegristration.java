package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class Receive_MessageCodeRegristration extends AppCompatActivity implements View.OnClickListener{

    private TextView counter, resend_msg, other_msg;
    private Button next;
    private ImageView loader_circle;
    private RelativeLayout relativeLayout;
    private final int countTimer= 120000;  //équivaut à 2 minutes

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_receive__message_code_regristration);

        resend_msg= findViewById(R.id.resend_msg);
        other_msg= findViewById(R.id.other_num);
        next= findViewById(R.id.btn_next2);

        resend_msg.setOnClickListener(this);
        other_msg.setOnClickListener(this);

    //animation du circle loader en attente du message code
        loader_circle= findViewById(R.id.loading_animation);
        Animation load_anim= AnimationUtils.loadAnimation(this, R.anim.anim_loader);
        loader_circle.startAnimation(load_anim);

        //pour afficher un counter de deux minutes
        counter= findViewById(R.id.countTimer);
        new CountDownTimer(countTimer,1000 ) {
            @Override
            public void onTick(long millisUntilFinished) {
                int minutes= (int) (millisUntilFinished/60000);
                int secondes= (int) (millisUntilFinished % 60000 / 1000);
                String time= "0"+minutes+":"+secondes;
                counter.setText(time);


            }

            @Override
            public void onFinish() {
                counter.setVisibility(View.INVISIBLE);
                loader_circle.setImageResource(R.drawable.done_cirle);
                Animation load_anim= AnimationUtils.loadAnimation(getApplicationContext(), R.anim.done_circle);
                loader_circle.startAnimation(load_anim);


            }
        }.start();

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){

            case R.id.resend_msg:
                Intent intent= new Intent(getApplicationContext(), Register_Number.class);
                startActivity(intent);
            case R.id.other_num:
                Intent intent2= new Intent(getApplicationContext(), Register_Number.class);
                startActivity(intent2);

            case R.id.btn_next2:
                Intent intent3= new Intent(getApplicationContext(), ArtiRegister.class);
                startActivity(intent3);

        }
    }
}
