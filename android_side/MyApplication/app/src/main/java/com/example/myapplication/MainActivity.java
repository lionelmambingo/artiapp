package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private Button professionnel;
    private Button client;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        professionnel= findViewById(R.id.professionnel);
        client= findViewById(R.id.client);
        professionnel.setOnClickListener(this);
        client.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.professionnel:
                Intent moveTosliders= new Intent(this, Register_Number.class);
                startActivity(moveTosliders);
            case R.id.client:
                Intent moveTosliders2= new Intent(this, Register_Number.class);
                startActivity(moveTosliders2);

        }
    }
}
