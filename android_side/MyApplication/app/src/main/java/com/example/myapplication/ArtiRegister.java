package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.transition.Explode;
import android.transition.Scene;
import android.transition.Slide;
import android.transition.Transition;
import android.transition.TransitionManager;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

public class ArtiRegister extends AppCompatActivity implements View.OnClickListener {

     View scene;
    private Boolean pict_click=true;

    private FloatingActionButton takepic;
    private Button register;
    ViewGroup root;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_arti_register);

        root= findViewById(R.id.scene_root);
        scene= findViewById(R.id.translate_pic);
        takepic= findViewById(R.id.take_pic);
        scene.setVisibility(View.INVISIBLE);
        register= findViewById(R.id.finish_register);
        register.setOnClickListener(this);
        takepic.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){

            case R.id.take_pic:

                if(pict_click){
                    Transition transition= new Slide(Gravity.TOP);
                    transition.addTarget(R.id.translate_pic);
                    TransitionManager.beginDelayedTransition(root, transition);
                    scene.setVisibility(View.VISIBLE);
                    pict_click=false;
                }else{
                    scene.setVisibility(View.GONE);
                    pict_click=true;

                }

                break;

            case R.id.finish_register:
                Intent intent = new Intent(getApplicationContext(), ArtiList.class);
                startActivity(intent);
        }

    }
}
