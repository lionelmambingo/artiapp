package com.example.myapplication;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;


// l'adaper qui va contenir deux constructeurs, le premier qui créé la  vue, la relie aux elements et la
//deuxième qui relie chaque element du fragment à la vue.
public class ArtiList_CustomAdapter extends RecyclerView.Adapter<ArtiList_CustomAdapter.MyViewHolder> {

    private List<UserDataModel> DataList;

    public ArtiList_CustomAdapter(List<UserDataModel> DataList){

        this.DataList= DataList;

    }

    //ici on crée la vue, on relie le recyclerview avec le cardview ou le fragment
    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.artilist_cardview, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        UserDataModel dataModel= DataList.get(position);
        //holder.nom.setText(dataModel.getNom());
        holder.city.setText(dataModel.getCity());
        holder.profession.setText(dataModel.getProfession());
        //holder.image_user.setImageBitmap(dataModel.getImage());



    }

    //ici on compte le nombre de models de données qu'on a c-à-d le nombre d'elements sur le recycler
    @Override
    public int getItemCount() {
        return DataList.size();
    }


    // le ViewHolder comme son nom l'indique relie toutes les vues qui sont dans notre RecyclerView texte, boutton etc...
    public class MyViewHolder extends RecyclerView.ViewHolder{
        public TextView nom, profession, city;
        //public ImageView image_user;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            //nom= itemView.findViewById(R.id.name_user_list);
            this.profession= itemView.findViewById(R.id.profession_userlist);
            this.city= itemView.findViewById(R.id.city_user_list);
            //image_user= itemView.findViewById(R.id.img_user_list);


        }
    }
}
