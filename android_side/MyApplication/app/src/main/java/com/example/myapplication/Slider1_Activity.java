package com.example.myapplication;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class Slider1_Activity extends FragmentActivity implements ViewPager.OnPageChangeListener, ViewPager.OnClickListener {


    private ViewPager mPager;
    private TextView next, pre;
    private PagerAdapter myAdapter;
    private final int NUMBER_PAGES= 3;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_slider1_);
        next= findViewById(R.id.suivant);
        pre= findViewById(R.id.precedant);
        mPager= (ViewPager) findViewById(R.id.viewpager);
        myAdapter= new FragmentstateAdapter(getSupportFragmentManager());
        mPager.setAdapter(myAdapter);
        mPager.setSaveEnabled (false);

        mPager.addOnPageChangeListener(this);
        //next.setOnClickListener(this);
        pre.setOnClickListener(this);


    }


    protected void onResume() {

        super.onResume();
    }


    protected void onPause() {

        super.onPause();
    }


    @Override
    public void onBackPressed() {
        if (mPager.getCurrentItem() == 0) {
            // If the user is currently looking at the first step, allow the system to handle the
            // Back button. This calls finish() on this activity and pops the back stack.
            super.onBackPressed();
        } else {
            // Otherwise, select the previous step.
            mPager.setCurrentItem(mPager.getCurrentItem() - 1);
        }

    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

        if (position== NUMBER_PAGES-1){

           next.setTextColor(getResources().getColor(R.color.colorPrimary));
           next.setClickable(true);
           next.setOnClickListener(new View.OnClickListener() {
               @Override
               public void onClick(View v) {
                   Intent intent= new Intent(getApplicationContext(), MainActivity.class);
                   startActivity(intent);

               }
           });
        }else{
            next.setTextColor(getResources().getColor(R.color.colorAccent));
            next.setClickable(false);

        }


    }

    @Override
    public void onPageSelected(int position) {

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){

            case R.id.precedant:
                if (mPager.getCurrentItem() == 0) {
                    // If the user is currently looking at the first step, allow the system to handle the
                    // Back button. This calls finish() on this activity and pops the back stack.
                    mPager.setCurrentItem(mPager.getCurrentItem() - 1);
                    //pre.setClickable(false);
                } else {
                    // Otherwise, select the previous step.
                    mPager.setCurrentItem(mPager.getCurrentItem() - 1);
                }
        }
    }




    private class FragmentstateAdapter extends FragmentStatePagerAdapter{

        private FragmentstateAdapter(FragmentManager fa){
            super(fa);

        }

        @Override
        public Fragment getItem(int position) {
            switch (position){

                case 0:
                    return new Slider1();
                case 1:
                    return new intro_slider2();
                case 2 :
                    return new Intro_Slider3();


            }
            return null;
        }

        @Override
        public int getCount() {
            return NUMBER_PAGES;
        }
    }
}
