const { GraphQLServer } = require('graphql-yoga')
const { prisma } = require("../database/generated/prisma-client");
const resolvers = require("./resolvers");
require("dotenv").config();

let option = {
  port: process.env.PORT
}

// connexion au serveur grapql il a besoin du schema des resolvers et du context généré par prisma 
const server = new GraphQLServer({
  typeDefs:"database/schema.graphql",
  resolvers,
  context: req => ({ ...req, prisma })
}) 
server.start(() => console.log(`Server is running on http://localhost:${option.port}`))






