const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')
const {getUserId } = require('./utils')

const Query= {
    //requete de consultation de toute la liste des proféssionnels de l'application 
    async  allprofessionals(parent, args, ctx, info){
        
        return await ctx.prisma.utilisateurs({
            where:{
                identite: args.identite
            }
        })
        
        
    },

    //requete pour filtrer sur un professionnel précis en fonction soit du nom, de la profession du ou metier 
    async  Lookingforprofessionnal(parent, args, ctx, info){
        
        return await ctx.prisma.utilisateur({
            id: args.id
        })
        
        
    },

//requete de consultation du code apres envoi du sms pour validation à l'étape de vérification 
async queryCode (parent, args,ctx, info){
    const code=  await ctx.prisma.codes({
        where:{
        number: args.number,
        code: args.code

        }
        
    })

    if (code){
        return code
        //console.log('le numéro est bien vérifié merci pour la patience')
    }
    throw new console.error('problème lors de la vérification merci de reessayer');
    

    }

}




module.exports= {Query}