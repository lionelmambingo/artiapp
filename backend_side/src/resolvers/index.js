const {Query} = require("./Query")

// Then the mutations
const { sendMessageCode } = require("./Mutations/sendMessageCode");
const { code, utilisateur } = require("./Mutations/relationsResolvers");
const { login, signUp } = require("./Mutations/Authentication");
module.exports = {
  Query,
  Mutation: {
    ...sendMessageCode,
    signUp
  },
  
  
};
