


    async function code (parent, context, info){
        return await context.prisma.Utilisateur({id: parent.id}).code()

    }
    
    async function utilisateur(parent, context, info){
        return await context.prisma.Code({id: parent.id}).utilisateur()

    }


module.exports = {code, utilisateur}