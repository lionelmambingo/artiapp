//cette partie est la mutation qui me permet d'envoyer un sms par le biais de l'api de twilio 

const {sendMessage}= require('../utils')

//generation d'un nombr à 6 chiffres aléatoire pour envoi du sms
const code= Math.floor(Math.random() * 999999) + 000000;

const sendMessageCode=  {
    //mutation for passing message to sms gateway 
    async sendMessageCode(parent, args, ctx, info){
        const number= args.number
        sendMessage(code, number)

        if (sendMessage){
             return await ctx.prisma.createCode({
                number: args.number,
                code: args.code,
                status: "true"
            })

        }else{
            console.log('le message n est pas parti merci de ressayer ')
        }
        

    }

}


module.exports ={sendMessageCode}