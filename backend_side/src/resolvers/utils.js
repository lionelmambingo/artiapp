//util.js est le module qui contient mes codes aléatoires qui me permettent entre autre de vérifier le user ou de genere l'api de twilio entre autre 

const twilio= require ('twilio') 
const jwt= require('jsonwebtoken')
require('dotenv').config()

const validate= (context)=>{
  
 
}

//la variable permet de genere un usertId à partir d'un token pris dans le header de la requete chaque fois que le user se sigIn ou se LogIn
const getUserId= (context)=>{
  const Authorization = context.request.get('Authorization')
  if (Authorization) {
    const token = Authorization.replace('Bearer ', '')
    const { userId } = jwt.verify(token, process.env.APP_SECRET)
    return userId
  }

  throw new Error('Utilisateur non authentifié')

}

//api de twilio pour envoi de sms au destinataire 
const sendMessage= (code, number)=>{
  var accountSid = 'AC3626d2b25252402aa1fec72ad2f88202'; // Your Account SID from www.twilio.com/console
  var authToken = '46b751d388a32930db9ad90211f4bbee';   // Your Auth Token from www.twilio.com/console

  var client = new twilio(accountSid, authToken);
  
  client.messages.create({
      body: 'Hello votre code est le '+code+ ' merci de me faire un retour',
      to: number,  // Text this number
      from: '+13608136402' // From a valid Twilio number
  })
  .then((message) => console.log(message.sid));


}

module.exports= {sendMessage, getUserId, validate}

  