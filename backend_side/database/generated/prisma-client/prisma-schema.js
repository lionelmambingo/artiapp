module.exports = {
        typeDefs: // Code generated by Prisma (prisma@1.34.10). DO NOT EDIT.
  // Please don't change this file manually but run `prisma generate` to update it.
  // For more information, please read the docs: https://www.prisma.io/docs/prisma-client/

/* GraphQL */ `type AggregateCode {
  count: Int!
}

type AggregateUtilisateur {
  count: Int!
}

type BatchPayload {
  count: Long!
}

type Code {
  id: ID!
  number: Int
  code: Int
  status: Boolean
  utilisateur: Code
}

type CodeConnection {
  pageInfo: PageInfo!
  edges: [CodeEdge]!
  aggregate: AggregateCode!
}

input CodeCreateInput {
  id: ID
  number: Int
  code: Int
  status: Boolean
  utilisateur: CodeCreateOneInput
}

input CodeCreateOneInput {
  create: CodeCreateInput
  connect: CodeWhereUniqueInput
}

type CodeEdge {
  node: Code!
  cursor: String!
}

enum CodeOrderByInput {
  id_ASC
  id_DESC
  number_ASC
  number_DESC
  code_ASC
  code_DESC
  status_ASC
  status_DESC
}

type CodePreviousValues {
  id: ID!
  number: Int
  code: Int
  status: Boolean
}

type CodeSubscriptionPayload {
  mutation: MutationType!
  node: Code
  updatedFields: [String!]
  previousValues: CodePreviousValues
}

input CodeSubscriptionWhereInput {
  mutation_in: [MutationType!]
  updatedFields_contains: String
  updatedFields_contains_every: [String!]
  updatedFields_contains_some: [String!]
  node: CodeWhereInput
  AND: [CodeSubscriptionWhereInput!]
  OR: [CodeSubscriptionWhereInput!]
  NOT: [CodeSubscriptionWhereInput!]
}

input CodeUpdateDataInput {
  number: Int
  code: Int
  status: Boolean
  utilisateur: CodeUpdateOneInput
}

input CodeUpdateInput {
  number: Int
  code: Int
  status: Boolean
  utilisateur: CodeUpdateOneInput
}

input CodeUpdateManyMutationInput {
  number: Int
  code: Int
  status: Boolean
}

input CodeUpdateOneInput {
  create: CodeCreateInput
  update: CodeUpdateDataInput
  upsert: CodeUpsertNestedInput
  delete: Boolean
  disconnect: Boolean
  connect: CodeWhereUniqueInput
}

input CodeUpsertNestedInput {
  update: CodeUpdateDataInput!
  create: CodeCreateInput!
}

input CodeWhereInput {
  id: ID
  id_not: ID
  id_in: [ID!]
  id_not_in: [ID!]
  id_lt: ID
  id_lte: ID
  id_gt: ID
  id_gte: ID
  id_contains: ID
  id_not_contains: ID
  id_starts_with: ID
  id_not_starts_with: ID
  id_ends_with: ID
  id_not_ends_with: ID
  number: Int
  number_not: Int
  number_in: [Int!]
  number_not_in: [Int!]
  number_lt: Int
  number_lte: Int
  number_gt: Int
  number_gte: Int
  code: Int
  code_not: Int
  code_in: [Int!]
  code_not_in: [Int!]
  code_lt: Int
  code_lte: Int
  code_gt: Int
  code_gte: Int
  status: Boolean
  status_not: Boolean
  utilisateur: CodeWhereInput
  AND: [CodeWhereInput!]
  OR: [CodeWhereInput!]
  NOT: [CodeWhereInput!]
}

input CodeWhereUniqueInput {
  id: ID
}

scalar Long

type Mutation {
  createCode(data: CodeCreateInput!): Code!
  updateCode(data: CodeUpdateInput!, where: CodeWhereUniqueInput!): Code
  updateManyCodes(data: CodeUpdateManyMutationInput!, where: CodeWhereInput): BatchPayload!
  upsertCode(where: CodeWhereUniqueInput!, create: CodeCreateInput!, update: CodeUpdateInput!): Code!
  deleteCode(where: CodeWhereUniqueInput!): Code
  deleteManyCodes(where: CodeWhereInput): BatchPayload!
  createUtilisateur(data: UtilisateurCreateInput!): Utilisateur!
  updateUtilisateur(data: UtilisateurUpdateInput!, where: UtilisateurWhereUniqueInput!): Utilisateur
  updateManyUtilisateurs(data: UtilisateurUpdateManyMutationInput!, where: UtilisateurWhereInput): BatchPayload!
  upsertUtilisateur(where: UtilisateurWhereUniqueInput!, create: UtilisateurCreateInput!, update: UtilisateurUpdateInput!): Utilisateur!
  deleteUtilisateur(where: UtilisateurWhereUniqueInput!): Utilisateur
  deleteManyUtilisateurs(where: UtilisateurWhereInput): BatchPayload!
}

enum MutationType {
  CREATED
  UPDATED
  DELETED
}

interface Node {
  id: ID!
}

type PageInfo {
  hasNextPage: Boolean!
  hasPreviousPage: Boolean!
  startCursor: String
  endCursor: String
}

type Query {
  code(where: CodeWhereUniqueInput!): Code
  codes(where: CodeWhereInput, orderBy: CodeOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): [Code]!
  codesConnection(where: CodeWhereInput, orderBy: CodeOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): CodeConnection!
  utilisateur(where: UtilisateurWhereUniqueInput!): Utilisateur
  utilisateurs(where: UtilisateurWhereInput, orderBy: UtilisateurOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): [Utilisateur]!
  utilisateursConnection(where: UtilisateurWhereInput, orderBy: UtilisateurOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): UtilisateurConnection!
  node(id: ID!): Node
}

type Subscription {
  code(where: CodeSubscriptionWhereInput): CodeSubscriptionPayload
  utilisateur(where: UtilisateurSubscriptionWhereInput): UtilisateurSubscriptionPayload
}

type Utilisateur {
  id: ID!
  nom: String!
  prenom: String!
  number: String!
  identite: String!
  profession: String!
  password: String!
  code: Code
}

type UtilisateurConnection {
  pageInfo: PageInfo!
  edges: [UtilisateurEdge]!
  aggregate: AggregateUtilisateur!
}

input UtilisateurCreateInput {
  id: ID
  nom: String!
  prenom: String!
  number: String!
  identite: String!
  profession: String!
  password: String!
  code: CodeCreateOneInput
}

type UtilisateurEdge {
  node: Utilisateur!
  cursor: String!
}

enum UtilisateurOrderByInput {
  id_ASC
  id_DESC
  nom_ASC
  nom_DESC
  prenom_ASC
  prenom_DESC
  number_ASC
  number_DESC
  identite_ASC
  identite_DESC
  profession_ASC
  profession_DESC
  password_ASC
  password_DESC
}

type UtilisateurPreviousValues {
  id: ID!
  nom: String!
  prenom: String!
  number: String!
  identite: String!
  profession: String!
  password: String!
}

type UtilisateurSubscriptionPayload {
  mutation: MutationType!
  node: Utilisateur
  updatedFields: [String!]
  previousValues: UtilisateurPreviousValues
}

input UtilisateurSubscriptionWhereInput {
  mutation_in: [MutationType!]
  updatedFields_contains: String
  updatedFields_contains_every: [String!]
  updatedFields_contains_some: [String!]
  node: UtilisateurWhereInput
  AND: [UtilisateurSubscriptionWhereInput!]
  OR: [UtilisateurSubscriptionWhereInput!]
  NOT: [UtilisateurSubscriptionWhereInput!]
}

input UtilisateurUpdateInput {
  nom: String
  prenom: String
  number: String
  identite: String
  profession: String
  password: String
  code: CodeUpdateOneInput
}

input UtilisateurUpdateManyMutationInput {
  nom: String
  prenom: String
  number: String
  identite: String
  profession: String
  password: String
}

input UtilisateurWhereInput {
  id: ID
  id_not: ID
  id_in: [ID!]
  id_not_in: [ID!]
  id_lt: ID
  id_lte: ID
  id_gt: ID
  id_gte: ID
  id_contains: ID
  id_not_contains: ID
  id_starts_with: ID
  id_not_starts_with: ID
  id_ends_with: ID
  id_not_ends_with: ID
  nom: String
  nom_not: String
  nom_in: [String!]
  nom_not_in: [String!]
  nom_lt: String
  nom_lte: String
  nom_gt: String
  nom_gte: String
  nom_contains: String
  nom_not_contains: String
  nom_starts_with: String
  nom_not_starts_with: String
  nom_ends_with: String
  nom_not_ends_with: String
  prenom: String
  prenom_not: String
  prenom_in: [String!]
  prenom_not_in: [String!]
  prenom_lt: String
  prenom_lte: String
  prenom_gt: String
  prenom_gte: String
  prenom_contains: String
  prenom_not_contains: String
  prenom_starts_with: String
  prenom_not_starts_with: String
  prenom_ends_with: String
  prenom_not_ends_with: String
  number: String
  number_not: String
  number_in: [String!]
  number_not_in: [String!]
  number_lt: String
  number_lte: String
  number_gt: String
  number_gte: String
  number_contains: String
  number_not_contains: String
  number_starts_with: String
  number_not_starts_with: String
  number_ends_with: String
  number_not_ends_with: String
  identite: String
  identite_not: String
  identite_in: [String!]
  identite_not_in: [String!]
  identite_lt: String
  identite_lte: String
  identite_gt: String
  identite_gte: String
  identite_contains: String
  identite_not_contains: String
  identite_starts_with: String
  identite_not_starts_with: String
  identite_ends_with: String
  identite_not_ends_with: String
  profession: String
  profession_not: String
  profession_in: [String!]
  profession_not_in: [String!]
  profession_lt: String
  profession_lte: String
  profession_gt: String
  profession_gte: String
  profession_contains: String
  profession_not_contains: String
  profession_starts_with: String
  profession_not_starts_with: String
  profession_ends_with: String
  profession_not_ends_with: String
  password: String
  password_not: String
  password_in: [String!]
  password_not_in: [String!]
  password_lt: String
  password_lte: String
  password_gt: String
  password_gte: String
  password_contains: String
  password_not_contains: String
  password_starts_with: String
  password_not_starts_with: String
  password_ends_with: String
  password_not_ends_with: String
  code: CodeWhereInput
  AND: [UtilisateurWhereInput!]
  OR: [UtilisateurWhereInput!]
  NOT: [UtilisateurWhereInput!]
}

input UtilisateurWhereUniqueInput {
  id: ID
}
`
      }
    